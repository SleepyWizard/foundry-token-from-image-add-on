Hooks.on("canvasReady", () => {
	canvas.tokens._onClickLeft2 = (event) => {
		if (event?.target?.actor.permission === 0) {
			let token = event.target;
			new ImagePopout(token.data.img, {
				title: token.name,
				shareable: false,
				uuid: token.uuid
				}).render(true);
		}
	}
});